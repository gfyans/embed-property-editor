﻿namespace Dawoe.OEmbedPickerPropertyEditor
{
    /// <summary>
    /// The constants.
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// The property editor alias.
        /// </summary>
        public const string PropertyEditorAlias = "Dawoe.OEmbedPickerPropertyEditor";

        /// <summary>
        /// The data type cache key.
        /// </summary>
        public const string DataTypeCacheKey = "Dawoe.OEmbedPickerPropertyEditor.AllowMultiple_{0}";

        /// <summary>
        /// The allow multiple prevalue.
        /// </summary>
        public const string AllowMultiplePrevalue = "allowmultiple";

        /// <summary>
        /// The resource folder.
        /// </summary>
        public const string ResourceFolder = "App_Plugins/DawoeOEmbedPickerPropertyEditor";
    }
}
